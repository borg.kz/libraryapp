﻿namespace Library.DAL.Auth {
	public class LoginModel {
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}
