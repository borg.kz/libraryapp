﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL {
	public class EntityBase : IEntity {
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
		public Guid Uid { get; set; } = Guid.NewGuid();

		[Required]
		[Column(TypeName = "timestamp")]
		[Timestamp]
		public byte[] Version { get; set; }
	}
}