﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Library.DAL.Model;
using Microsoft.AspNetCore.Identity;

namespace Library.DAL.Auth {
	public class MUser : IdentityUser, IEntity {
		[Column(TypeName = "nvarchar(150)")]
		public string FullName { get; set; }

		/// <summary>
		/// Library User Id, will use for finding of user
		/// Format: U00001
		/// </summary>
		[Column(TypeName = "nvarchar(6)")]
		public string LibraryUserId { get; set; }

		[InverseProperty("Client")]
		public List<MBorrowingHistory> BorrowingHistory { get; set; }

		[InverseProperty("Manager")]
		public List<MBorrowingHistory> LendingHistory { get; set; }

		// TODO Arch#6: for this class it properties are redudant, we can remove them from interface 
		[NotMapped]
		public Guid Uid { get; set; }
		[NotMapped]
		public byte[] Version { get; set; }
	}
}