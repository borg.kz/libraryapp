﻿using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Remote;
using System;
using System.Diagnostics;
using System.Drawing;
using TechTalk.SpecFlow;

namespace Epic.SCF.QA.Core.Engine
{
    public class WebDriver
    {
        private const int WAIT_SECONDS = 8;
        private bool _disposed = false;
        private IJavaScriptExecutor _jsExecutor;
        private Actions _builder;

        private Type[] typesToIgnore = new Type[]{
            typeof(ArgumentException),
            typeof(NoSuchElementException),
            typeof(NotImplementedException),
            typeof(InvalidOperationException),
            typeof(ElementNotVisibleException),
            typeof(StaleElementReferenceException)
        };

        public IWebDriver Browser { get; private set; }
        public Stopwatch StopWatch { get; set; }
        public EBrowserType Type { get; }

        public WebDriver()
        {
            Type = EBrowserType.IE;
            Initialize();
        }

        #region Initialisation

        ~WebDriver()
        {
            if (!_disposed)
                Dispose();
        }

        private void KillProcesses()
        {
            Process[] processes = Process.GetProcessesByName("IEDriverServer");
            Helper.Debug("Process length: " + processes.Length.ToString());
            foreach (Process process in processes)
            {
                Helper.Debug("Process to be killed: " + process.ToString());
                process.Kill();
            }
        }

        private void InitializeIE()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.EnableNativeEvents = FeatureContext.Current.ContainsKey("AuditLog");
            options.AddAdditionalCapability("disable-popup-blocking", true);
            options.EnsureCleanSession = true;
            options.RequireWindowFocus = true;
            options.PageLoadStrategy = PageLoadStrategy.Eager;
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            options.IgnoreZoomLevel = true;

            Browser = new InternetExplorerDriver(Environment.CurrentDirectory, options);
            Browser.Manage().Window.Size = new Size(1024, 768);
            ManageImplicitWait(30);
            _jsExecutor = Browser as IJavaScriptExecutor;

            _builder = new Actions(Browser);
        }

        private void Initialize()
        {
            switch (Type)
            {
                case EBrowserType.Chrome:
                    InitializeIE();
                    break;
                case EBrowserType.Edge:
                    InitializeIE();
                    break;
                case EBrowserType.IE:
                    InitializeIE();
                    break;
                case EBrowserType.FF:
                    Browser = new FirefoxDriver();
                    break;
                case EBrowserType.Opera:
                    Browser = new OperaDriver();
                    break;
                default:
                    Browser = new RemoteWebDriver(new RemoteSessionSettings());
                    break;
            }
            _builder = new Actions(Browser);
            _jsExecutor = Browser as IJavaScriptExecutor;
        }

        public void Restart()
        {
            Browser.Quit();
            Browser.Dispose();
            Browser = null;
            GC.Collect(2, GCCollectionMode.Forced);
            Initialize();
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            Browser.Quit();
            Browser.Dispose();
            Browser = null;
            KillProcesses();
            _disposed = true;
            GC.SuppressFinalize(this);
        }

        #endregion Initialisation

        public void ManageImplicitWait(int seconds = WAIT_SECONDS)
        {
            Browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(seconds);
        }
    }
}