import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { BookStoreService } from '../book-store.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-book-store-write-off',
  templateUrl: './book-store-write-off.component.html'
})
export class BookStoreWriteOffComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private pbookStoreService: BookStoreService) { }
  
  addForm: FormGroup;
  submitted = false;
  
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      Uid: [],
      Title: [localStorage.getItem("AddBook.title"), Validators.required],
      Isbn: [localStorage.getItem("AddBook.isbn"), Validators.required],
      Reason: ['', Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    
    if(this.addForm.valid){
      this.pbookStoreService.writingOffBook(this.addForm.value)
      .subscribe( data => {
        console.log(data);
        // We can check answer here, response have to have 200 status code, 
        // and we can show Error message from server, if not 200 code is happened 
        // gloal function CheckStatus(data)
        this.router.navigate(['bookstore']);
      });
    }
  }

  // get the form short name to access the form fields
  get f() { return this.addForm.controls; }
}
