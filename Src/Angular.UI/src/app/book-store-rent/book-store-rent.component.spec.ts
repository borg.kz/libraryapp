import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookStoreRentComponent } from './book-store-rent.component';

describe('BookStoreRentComponent', () => {
  let component: BookStoreRentComponent;
  let fixture: ComponentFixture<BookStoreRentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookStoreRentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreRentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
