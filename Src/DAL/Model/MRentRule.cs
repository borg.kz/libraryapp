﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	[Table("Library_RentRule")]
	public class MRentRule : EntityBase {
		public string Name{ get; set; }
		public float FirstCost{ get; set; }
		public int FirstCostPeriod{ get; set; }
		public float SecondCost{ get; set; }
		public int SecondCostPeriod{ get; set; }
		public float ThirdCost{ get; set; }
		public int ThirdCostPeriod{ get; set; }

		/// <summary> Book is free </summary>
		private bool IsFree { get; set; }
		private bool IsActive { get; set; }

		[Required]
		public ICollection <MBook> Books { get; set; }

		[Required]
		public ICollection <MBorrowingHistory> Borrowers { get; set; }
	}
}