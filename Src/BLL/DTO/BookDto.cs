﻿using System.Linq;
using Library.DAL.Model;

namespace Library.BLL.DTO {
	public class BookDto {
		public BookDto() { }

		public BookDto(MBook book) {
			Uid = book.Uid.ToString();
			Title = book.Title;
			Isbn = book.ISBN;
			Publisher = string.Join(", ", book.Publisher, book.PublishingDate.ToShortDateString());
			Description = book.Description;
			// Autors = string.Join(", ", book.Autors?.Select(n => n.Name));
			Authors = "Somebody Great";
			Quantity = book.Quantity;
		}

		public string Uid { get; set; }

		public string Title { get; set; }

		public string Isbn { get; set; }

		public string Publisher { get; set; }

		public string PublishDate { get; set; }

		public string Description { get; set; }

		public string Authors { get; set; }

		public string RentRule { get; set; }

		public int Quantity { get; set; }


	}
}