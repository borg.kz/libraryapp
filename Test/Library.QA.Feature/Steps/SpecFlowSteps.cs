﻿using BoDi;
using Epic.SCF.QA.Core.Engine;
using System;
using TechTalk.SpecFlow;

namespace Epic.SCF.QA.Infrastructur.Feature
{
    // TODO: We can create Before/After Feature, Before/After Scenario, Before/After step action here
    // TODO: SpecFlow issue https://github.com/techtalk/SpecFlow/issues/1315
    /// <summary>
    /// Only for system settings
    /// </summary>
    [Binding]
    public class SpecFlowSteps
    {
        private WebDriver _engine;
        private readonly IObjectContainer _context;
        private string _pageUrl = string.Empty;

        public SpecFlowSteps(IObjectContainer container)
        {
            _context = container;
        }

        // For now we will create a new instance of Browser for each Scenario
        [BeforeScenario(Order = 0)]
        public void RunBeforeScenario()
        {
            if (_engine == null)
                _engine = new WebDriver();

            _context.RegisterInstanceAs<WebDriver>(_engine);
        }

        [AfterScenario(Order = Int32.MaxValue)]
        public void AfterFeature()
        {
            if (_engine == null)
                return;

            _engine.Dispose();
            _engine = null;
        }
    }
}
