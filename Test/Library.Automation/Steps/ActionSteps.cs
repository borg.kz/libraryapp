﻿using Librariy.Automation.Core;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using TechTalk.SpecFlow;

namespace Library.Automation.Test.Steps {
	public partial class SpecFlowSteps {
		/// <summary>
		/// Click on DOM element
		/// </summary>
		/// <param name="element">Id of DOM element</param>
		[StepDefinition(@"@ACTION: click '(\S*)'")]
		public void ActionClickOnElement(string element) {

		}

    }

	public static class Ext	{
        public static void DoubleClickOnElement(this IWebElement onElement, Engine engine)
        {
            new Actions(engine.Browser).DoubleClick(onElement).Build().Perform();
            // _engine.WaitPageIsLoaded(5);
        }
    }
}