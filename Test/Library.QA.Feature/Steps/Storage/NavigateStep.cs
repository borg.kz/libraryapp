﻿using Epic.SCF.QA.Core.Engine;
using Epic.SCF.QA.DAL;
using System.Linq;
using TechTalk.SpecFlow;

namespace Epic.SCF.QA.Infrastructur.Feature.Storage
{
    /// <summary>
    /// Navigation step
    /// </summary>
    [Binding]
    public class NavigateStep
    {
        private WebDriver _engine;

        public NavigateStep(WebDriver engine)
        {
            _engine = engine;
        }

        [Given(@"NAVIGATE: Enter to Google")]
        public void EnterToApplication()
        {
            _engine.Browser.Navigate().GoToUrl("Google.com");
        }

        [Given(@"NAVIGATE: to URL '(http:\\.*)'")]
        public void NavigateToUrl(string url)
        {
            _engine.Browser.Navigate().GoToUrl(url);
        }

        [Given(@"NAVIGATE: TestPage")]
        public void TestPage()
        {
            _engine.Browser.Navigate().GoToUrl("yandex.ru");
        }

        [Given(@"NAVIGATE: Test EntityFramework")]
        public void TestEF()
        {
            var context = new ScfDbContext();
            var codeRows = context.CodeRow.Where(c => c.Active == true && c.MunicipalityId == "092").ToList();
            var invoices = context.Invoice.Where(i => i.ToString() == "");

            if (codeRows.Count < 1000)
                throw new System.Exception();

            context.SaveChanges();
        }
    }
}
