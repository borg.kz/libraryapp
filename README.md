# LibraryApp

* ASP.NET Core 2
* Entity Framework Code First Core
* AspNetCore.Authentication.JwtBearer
* Dependency Injection
* Unit of work
    * Repository
    * Observers which are monitoring data changes in repositories
    * Optimistic locking
* Angular 7
    * Login dialog

## Work plan:
**WebApi Arch**
* [X]  Create project and add libraries
* [X]  Create DB Model
* [X]  Create DB structure (Migration + scripts)
* [X]  Add JwtBearer framework for Authorization
* [X]  Implement Uow pattern
* [X]  Add observers for changing data in repositories
* [X]  Implement optimistic locking data changes
* [X]  Create and describe RESTfull servises 
* [X]  Implement Memory caching
* [X]  Implement IServiceManager infrastructure to perform busines-requirements 
* [X]  Implement IRentRule mechanism
* [ ]  Add AutoMapper!!!!!
* [ ]  Add swagger
* [X]  Implement unit tests
* [X]  Implement Selenuim + SpeckFlow test approach


**Angular**
* [X]  Create application 
* [X]  Implement login component [P1]
* [X]  Create Home screen 
* [X]  Implement Arrival new books creen [P1]
* [X]  Implenemt rented books screen [P1]
* [ ]  Implement List of rented books screen [P1]
* [ ]  Implement return borrowing screen [P1]
* [ ]  Implement writing off books from stock [P2]
* [ ]  Implement search screen by book properties [P2]
* [ ]  Implement Draft mechanism for update/create screens [P2]
* [ ]  Generate EMail notify after X days renting [P3]
* [ ]  Implement Glossary by Autor name [P3]
* [ ]  Implement sibscribtion to borrowing book and get notify when it will apear in store [P3]
* [ ]  Implement sorting for search screen [P3]


For launch you have to add Roles to DB
Table: **Library_Book**

**ID**                                 **Name**    **NormalizedName**

0deb9e97-4ebb-4031-866f-cc3502d01c01	Manager	Manager	       
0deb9e97-4ebb-4031-866f-cc3502d01c06	Cllient	Client	        