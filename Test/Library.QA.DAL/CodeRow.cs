﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Epic.SCF.QA.DAL
{
    [Table("CodeRow")]
    public class CodeRow
    {
        public CodeRow()
        {
            IsConfigured = true;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CodeRowId { get; set; }

        public string CodeGroupId { get; set; }

        public string MunicipalityId { get; set; }

        public string Key { get; set; }

        public string Value { get; set; }

        public bool Active { get; set; }

        public string Comment { get; set; }

        [NotMapped]
        public bool IsConfigured { get; set; }
    }
}