﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Epic.SCF.QA.Core
{
    public enum EBrowserType
    {
        Chrome = 1,
        Edge = 2,
        IE = 3,
        FF = 4,
        Opera = 5,
        Remote = 6
    }
}
