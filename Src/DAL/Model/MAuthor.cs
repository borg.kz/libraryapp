﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	/// <summary>
	/// Author can have many books
	/// </summary>
	[Table("Library_Author")]
	public class MAuthor : EntityBase {
		public ICollection<MRelBookAuthor> BookAutor { get; set; }
		[Required]
		public string Name { get; set; }
		public string LastName { get; set; }
		public string FullName { get; set; }
	}
}