import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientBookSearchComponent } from './client-book-search.component';

describe('ClientBookSearchComponent', () => {
  let component: ClientBookSearchComponent;
  let fixture: ComponentFixture<ClientBookSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientBookSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientBookSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
