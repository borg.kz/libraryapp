﻿using System;
using System.Buffers;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Library.DAL.Core {
	public class DataChangeObserver {
		public ConcurrentDictionary<Type, List<Action<Guid, IEntity>>> ChangeEvents = new ConcurrentDictionary<Type, List<Action<Guid, IEntity>>>();

		public void AddEvent(IDataChangeEvent dataChangeEvent) {
			if (!ChangeEvents.TryGetValue(dataChangeEvent.Type, out var actions)) {
				ChangeEvents.TryAdd(dataChangeEvent.Type, new List<Action<Guid, IEntity>> { dataChangeEvent.Action });
				return;
			}

			ChangeEvents.TryGetValue(dataChangeEvent.Type, out var value);
			value?.Add(dataChangeEvent.Action);
		}

		public void RemoveEvent(IDataChangeEvent dataChangeEvent) {
			
		}

		public void ClearEvents<T>() where T : Type {
			
		}
	}
}