import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookStoreWriteOffComponent } from './book-store-write-off.component';

describe('BookStoreWriteOffComponent', () => {
  let component: BookStoreWriteOffComponent;
  let fixture: ComponentFixture<BookStoreWriteOffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookStoreWriteOffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreWriteOffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
