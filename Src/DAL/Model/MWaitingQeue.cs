﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library.DAL.Auth;

namespace Library.DAL.Model {
	[Table("Library_WaitingQeue")]
	public class MWaitingQeue : EntityBase {
		[Required]
		public MBook Book { get; set; }
		[Required]
		public MUser Client { get; set; }
		[Required]
		public DateTime CreationDate { get; set; }
	}
}