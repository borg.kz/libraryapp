﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BLL.DTO {
	public class ReturnBookDto {
		public string Isbn { get; set; }
		public string Lui { get; set; }
	}
}
