﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Library.BLL.DTO;
using Library.DAL;
using Library.DAL.Auth;
using Library.DAL.Core;
using Library.DAL.Model;
using Microsoft.EntityFrameworkCore;

namespace Library.BLL.Managers {
	/// <summary>
	/// This manager was designed against the first principle of SOLID, just for demonstration. It should be divided into 4 parts.
	/// 1. BookStore
	/// 2. Rents activities
	/// 3. WritingOff activities
	/// 4. Book search
	/// </summary>
	public class LibraryManager : ManagerBase {
		private readonly IRepositoryAsync<MBook> _repository;

		public LibraryManager(IUnitOfWork uow) : base(uow) {
            _ = uow ?? throw new ArgumentNullException(nameof(uow));
            _repository = uow.RepoAsync<MBook>();
		}

		/// <summary> Load all books </summary>
		public async Task<IEnumerable<BookDto>> GetBooksAsync(int? skip, int? take) {
			var result = (await _repository.Query(null, b => b.Include(r => r.RentRule))).ToList();

			return result.Select(b => new BookDto(b) {
				RentRule = GenerateRentDescription(b.RentRule)
			});
		}

		/// <summary> Get the Book details </summary>
		public async Task<BookDto> GetBookInfoAsync(string bookIid) {
			var result = new BookDto(await _repository.FirstOrDefault(b => b.Uid == Guid.Parse(bookIid)));
			return result;
		}

		/// <summary> Get the Borrowing Book details </summary>
		public async Task<BookDto> GetBorrowedBookInfoAsync(string bookUid, string lui) {
			var result = new BookDto(await _repository.FirstOrDefault(b => b.Uid == Guid.Parse(bookUid)));
			var borrowingRecord = await Uow.RepoAsync<MBorrowingHistory>()
				.FirstOrDefault(bh => bh.Book.Uid == Guid.Parse(bookUid) && bh.Client.LibraryUserId == lui);

			result.RentRule = GenerateRentDescription(borrowingRecord.RentRule);

			return result;
		}

		/// <summary> Update book properties </summary>
		public async Task<bool> UpdateBookAsync(BookDto obj) {
			if (obj == null) throw new ArgumentNullException(nameof(BookDto));

			var book = await _repository.FirstOrDefault(b => b.Uid == Guid.Parse(obj.Uid));
			book.Title = obj.Title;
			book.ISBN = obj.Isbn;
			book.Publisher = obj.Publisher;
			DateTime.TryParse(obj.PublishDate, out DateTime date);
			book.PublishingDate = date;

			await _repository.Commit();
			return true;
		}

		/// <summary> Add book to store </summary>
		public async Task<bool> AddBookAsync(BookDto obj) {
			if (obj == null) throw new ArgumentNullException(nameof(BookDto));

			var book = new MBook {
				Title = obj.Title,
				ISBN = obj.Isbn,
				Publisher = obj.Publisher,
				Description = obj.Description,
				Quantity = obj.Quantity,
				RentRule = await Uow.RepoAsync<MRentRule>().FirstOrDefault(r => r.Name == obj.RentRule)
			};
			
			_repository.MarkUnchanged(book.RentRule);

			DateTime.TryParse(obj.PublishDate, out DateTime date);
			book.PublishingDate = date;

			// Yep, it is very very veeeery bad implementation of authors but not have time for implementation of the full instance with name, last name, etc.
			var autors = obj.Authors.Split(';');
			AddAuthors(book, autors);
			await _repository.Create(book);
			await Uow.Commit();
			await AddStoryHistoryRecord(book);
			await Uow.Commit();

			return true;
		}

		/// <summary> Rent book </summary>
		public async Task<bool> RentBookAsync(RentBookDto obj, string managerId) {
			var userRepo = Uow.RepoAsync<MUser>();
			var client = await userRepo.FirstOrDefault(u => u.LibraryUserId == obj.Lui);
			if (client == null) 
				throw new ArgumentNullException(nameof(client));

			var book = await _repository.FirstOrDefault(b => b.ISBN == obj.Isbn);
			if (book.Quantity == 0)
				throw new Exception("There is no books in store");

			var borrowingRecord = new MBorrowingHistory() {
				Book = book,
				Client = client,
				DateOfGetting = DateTime.Now,
			};

			book.Quantity--;

			_repository.MarkUnchanged(borrowingRecord.Client);
			_repository.MarkUnchanged(borrowingRecord.Book);

			// We shouldn't decrease bookStoreRecord.Quantity, it have to do IDataChancheEvent thru general Observer

			await Uow.RepoAsync<MBorrowingHistory>().Create(borrowingRecord);
			await Uow.Commit();
			await _repository.Update(book);
			await Uow.Commit();

			return true;
		}

		public async Task<float> CostOfRentAsync(ReturnBookDto obj) {
			var borrpwingRecord = await Uow.RepoAsync<MBorrowingHistory>()
				.FirstOrDefault(bh => bh.Book.ISBN == obj.Isbn && bh.Client.LibraryUserId == obj.Lui, b => b.Include(r => r.RentRule));
			borrpwingRecord.DateOfReturning = DateTime.Now;
			return CalculateCostOfRent(borrpwingRecord);
		}

		// TODO: It is money transfer operation it should have 2 steps. 1 - display debt, 2 - commit date of returning
		/// <summary> Return the book to the store</summary>
		public async Task<bool> ReturnBookAsync(ReturnBookDto obj) {
			var borrpwingRecord = await Uow.RepoAsync<MBorrowingHistory>()
				.FirstOrDefault(bh => bh.Book.ISBN == obj.Isbn && bh.Client.LibraryUserId == obj.Lui, b => b.Include(r => r.RentRule));

			borrpwingRecord.DateOfReturning = DateTime.Now;
			
			var book = await _repository.FirstOrDefault(b => b.ISBN == obj.Isbn);
			book.Quantity++;

			await _repository.Update(book);
			await Uow.RepoAsync<MBorrowingHistory>().Update(borrpwingRecord);

			await Uow.Commit();

			return true;
		}

		private float CalculateCostOfRent(MBorrowingHistory borrowing) {
			var rentRule = Uow.RepoAsync<MRentRule>().FirstOrDefault(r => r.Name == "One").Result;
			var sumOfDays = borrowing.DateOfReturning.Value.DayOfYear - borrowing.DateOfGetting.DayOfYear;
			var debtForFirstWeek = 5 - (int)borrowing.DateOfGetting.DayOfWeek;
			var debtForLastWeek = 5 - (int)borrowing.DateOfReturning.Value.DayOfWeek;

			var rentDays = (sumOfDays - debtForLastWeek - debtForFirstWeek) / 7 * 5 + (debtForLastWeek + debtForFirstWeek);
			float result = 0;
			
			if (rentDays < rentRule.FirstCostPeriod) {
				return rentRule.FirstCost * rentDays;
			}

			result = rentRule.FirstCost * rentRule.FirstCostPeriod;
			rentDays -= rentRule.FirstCostPeriod;

			if (rentDays < rentRule.SecondCostPeriod) {
				return rentRule.SecondCost * rentDays + result;
			}

			result += rentRule.SecondCost * rentRule.SecondCostPeriod;
			rentDays -= rentRule.SecondCostPeriod;

			return rentRule.SecondCost * rentDays + result;
		}

		/// <summary> Writting off the book</summary>
		public async Task<bool> WrittingOffBookAsync(BookDto obj) {
			var book = await _repository.FirstOrDefault(b => b.Uid == Guid.Parse(obj.Uid));
			book.Quantity -= obj.Quantity;
			await _repository.Update(book);
			await _repository.Commit();

			var storeHistoryRecord = new MStoreHistory {
				Book = book,
				Count = book.Quantity,
				Date = DateTime.Now,
				TransactionType = ETransactionType.Losting
			};
			
			Uow.RepoAsync<MStoreHistory>().MarkUnchanged(storeHistoryRecord.Book);

			await Uow.RepoAsync<MStoreHistory>().Create(storeHistoryRecord);

			await Uow.Commit();

			return true;
		}

		/// <summary> Writting off the book</summary>
		public Task<bool> GetRenredBooksAsync(int? skip, int? take) {
			return Task.FromResult(true);
		}

		private async Task AddStoryHistoryRecord(MBook book) {
			var storeHistoryRecord = new MStoreHistory {
				Book = book,
				Count = book.Quantity,
				Date = DateTime.Now,
				TransactionType = ETransactionType.Arrival
			};
			await Uow.RepoAsync<MStoreHistory>().Create(storeHistoryRecord);

			await Uow.Commit();
		}

		private void AddAuthors(MBook book, string[] authors) {
			book.BookAutor = new List<MRelBookAuthor>();

			foreach (var authorName in authors) {
				var initials = authorName.Split(' ');
				var author = new MAuthor {
					Name = initials[0],
					LastName = initials.Length > 1 ? initials[1] : string.Empty,
					FullName = authorName
				};

				book.BookAutor.Add(new MRelBookAuthor {
					Book = book,
					Author = author
				});
			}
		}

		public async Task<IEnumerable<BookDto>> BookSearchAsync(BookSearchDto query) {
			Expression<Func<MBook, bool>> filter = a => a.Quantity > 0;
			if (!string.IsNullOrEmpty(query.Isbn))
				filter = Combine(filter, s => s.ISBN.Contains(query.Isbn), false);
			
			if (!string.IsNullOrEmpty(query.Publisher))
				filter = Combine(filter, s => s.ISBN.Contains(query.Publisher), false);
			
			if (!string.IsNullOrEmpty(query.Description))
				filter = Combine(filter, s => s.Description.Contains(query.Description), false);
			
			if (!string.IsNullOrEmpty(query.Author))
				filter = Combine(filter, s => s.BookAutor.Any(b => b.Author.FullName.Contains(query.Author)), false);

			var result = await _repository.Query(filter, b => b.Include(r => r.RentRule));

			return result.ToList().Select(b => new BookDto(b) {
				RentRule = GenerateRentDescription(b.RentRule)
			});
		}

		private string GenerateRentDescription(MRentRule rule) {
			if (rule == null)
				return string.Empty;

			return $"{rule.FirstCostPeriod} days are {rule.FirstCost}€ , then {rule.SecondCost}€ per day for the next {rule.SecondCostPeriod} days, afterwards {rule.ThirdCost}€ per day";
		}

		Expression<Func<T, bool>> Combine<T>(Expression<Func<T, bool>> filter1, Expression<Func<T, bool>> filter2, bool isOr = true) {
			// combine two predicates:
			// need to rewrite one of the lambdas, swapping in the parameter from the other
			var rewrittenBody1 = new ReplaceVisitor(
				filter1.Parameters[0], filter2.Parameters[0]).Visit(filter1.Body);
			var newFilter = isOr ? Expression.Lambda<Func<T, bool>>(
				Expression.Or(rewrittenBody1, filter2.Body), filter2.Parameters) : Expression.Lambda<Func<T, bool>>(
				Expression.And(rewrittenBody1, filter2.Body), filter2.Parameters);
			return newFilter;
		}

		private class ReplaceVisitor : ExpressionVisitor {
			private readonly Expression from, to;
			public ReplaceVisitor(Expression from, Expression to) {
				this.from = from;
				this.to = to;
			}

			public override Expression Visit(Expression node) {
				return node == from ? to : base.Visit(node);
			}
		}
	}
}