﻿using TechTalk.SpecFlow;
using Xunit;

namespace Library.Automation.Test.Steps {
	public partial class SpecFlowSteps {
		[StepDefinition(@"#CHECK: element '(\S*)' option '(\S*)' value '(\S*)'")]
		public void CheckElementByOption(string element, string option, string value) {
			
		}
		
		[StepDefinition(@"#CHECK: element '(\S*)' is diabled '(false|true)'")]
		public void CheckElementByOption(string elementId, bool value) {
			var element = _engine.GetElementById(elementId);
			Assert.True(element.Enabled != value, $"Element ({elementId}) have to be {(value ? "Disabled" : "Enabled")}");
		}
	}
}