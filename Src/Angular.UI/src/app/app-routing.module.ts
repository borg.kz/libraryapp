import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { AuthGuard } from './auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { StoreComponent } from './store/store.component';
import { BookStoreComponent } from './book-store/book-store.component';
import { BookStoreAddComponent } from './book-store-add/book-store-add.component';
import { BookStoreRentComponent } from './book-store-rent/book-store-rent.component';
import { BookStoreWriteOffComponent } from './book-store-write-off/book-store-write-off.component';
import { BookStoreReturnComponent } from './book-store-return/book-store-return.component';
import { ClientBookSearchComponent } from './client-book-search/client-book-search.component';

const routes: Routes = [
  { path:'', redirectTo:'/user/login', pathMatch:'full' },
  {
    path: 'user', component: UserComponent,
    children: [
      { path: 'registration', component: RegistrationComponent },
      { path: 'login', component: LoginComponent }
    ]
  },
//  {path:'home',component:HomeComponent},
  { path:'store', component: StoreComponent },
  { path:'bookstore', component: BookStoreComponent },
  { path:'app-book-store-add', component: BookStoreAddComponent },
  { path:'app-book-store-rent', component: BookStoreRentComponent },
  { path:'app-book-store-return', component: BookStoreReturnComponent },
  { path:'app-book-store-write-off', component: BookStoreWriteOffComponent },
  { path:'app-client-book-search', component: ClientBookSearchComponent },
  { path:'home', component: HomeComponent, canActivate: [AuthGuard] },
  { path:'free', component: HomeComponent },
  { path:'forbidden', component: ForbiddenComponent },
  { path:'adminpanel', component: AdminPanelComponent, canActivate: [AuthGuard], data: {permittedRoles:['Admin']} }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
