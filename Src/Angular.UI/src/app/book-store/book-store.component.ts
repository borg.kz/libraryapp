import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { BookModel } from '../Models/BookModel';
  import { BookStoreService } from '../book-store.service';

@Component({
  selector: 'app-book-store',
  templateUrl: './book-store.component.html'
})
export class BookStoreComponent implements OnInit {
  books: BookModel[];

  constructor(private bookStoreService: BookStoreService, private router: Router) { }

  ngOnInit() {
    this.getBooks();
  }

  getBooks(): void {
    this.bookStoreService.getBooks().subscribe(data => {
      this.books = data;
    });
  };

  addBook(): void {
    this.router.navigate(['app-book-store-add']);
  }

  writtingOffBook(book: BookModel): void {
    localStorage.removeItem("AddBook.uid");
    localStorage.removeItem("AddBook.title");
    localStorage.removeItem("AddBook.isbn");
    localStorage.setItem("AddBook.uid", book.Uid);
    localStorage.setItem("AddBook.title", book.Title);
    localStorage.setItem("AddBook.isbn", book.Isbn);

    this.router.navigate(['app-book-store-write-off']);
  }

  rentBook(book: BookModel) {
    localStorage.removeItem("AddBook.uid");
    localStorage.removeItem("AddBook.title");
    localStorage.removeItem("AddBook.isbn");
    localStorage.setItem("AddBook.uid", book.Uid);
    localStorage.setItem("AddBook.title", book.Title);
    localStorage.setItem("AddBook.isbn", book.Isbn);

    this.router.navigate(['app-book-store-rent']);
  }
}
