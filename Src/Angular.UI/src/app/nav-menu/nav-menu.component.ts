import { UserService } from './../shared/user.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html'
})
export class NavMenuComponent implements OnInit {

constructor(private router: Router) { }

    ngOnInit() {    }

    onLogout() {
      localStorage.removeItem('token');
      this.router.navigate(['/user/login']);
    }
}
