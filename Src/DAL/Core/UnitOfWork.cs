﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;
using Library.DAL.DataChangeEvents;
using Library.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Library.DAL.Core {
	public sealed class UnitOfWork<TContext> : IUnitOfWork<TContext>
		where TContext : DbContext, IDisposable {
		private readonly ConcurrentDictionary<Type, object> _repositories = new ConcurrentDictionary<Type, object>();
		private readonly DataChangeObserver _dataChangeObserver = new DataChangeObserver();

		public UnitOfWork(TContext context, IMemoryCache cache) {
			Cache = cache ?? throw new ArgumentNullException(nameof(cache));
			Context = context ?? throw new ArgumentNullException(nameof(context));

			RegisterObservers();
			if (!Cache.TryGetValue(EGlobalCacheKeys.RentRuleKey.ToString(), out object _)) {
				var repo = RepoAsync<MRentRule>().Query().Result.ToList();
				Cache.Set(EGlobalCacheKeys.RentRuleKey.ToString(), repo, new MemoryCacheEntryOptions {
					SlidingExpiration = TimeSpan.FromHours(1)
				});
			}
		}

		public IRepositoryAsync<TEntity> RepoAsync<TEntity>() where TEntity : class, IEntity {
			var type = typeof(TEntity);
			if (!_repositories.ContainsKey(type)) _repositories[type] = new RepositoryAsync<TEntity>(Context);

			var result = (IRepositoryAsync<TEntity>)_repositories[type];
			
			// TODO Arch#3: we can use Span<Action>() for improving performance

			if (_dataChangeObserver.ChangeEvents.ContainsKey(typeof(TEntity)))
				result.DataChangeActions = _dataChangeObserver.ChangeEvents[typeof(TEntity)];
			return result;
		}

		public TContext Context { get; }
		public IMemoryCache Cache { get; }

		public void Dispose() {
			Context?.Dispose();
		}

		public Task<int> Commit() {
			return Context.SaveChangesAsync();
		}

		private void RegisterObservers() {
			_dataChangeObserver.AddEvent(new QantityBookChange(this));
		}
	}
}