﻿using FluentAssertions;
using Library.BLL.Managers;
using Library.DAL.Core;
using Library.WebAPI.Controllers;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Library.Test.WebApi {
	public class LibraryControllerTest {
		private readonly LibraryController _controller;
		private readonly LibraryManager _manager;
		private readonly IUnitOfWork _uow;
		private readonly IRepositoryAsync<MBook> _repository;

		public LibraryControllerTest() {
			_repository = Substitute.For<IRepositoryAsync<MBook>>();
			_uow = Substitute.For<IUnitOfWork>();
			_uow.RepoAsync<MBook>().Returns(_repository);
			_manager = Substitute.For<LibraryManager>(_uow);

			_controller = new LibraryController(_manager);
		}

		[Fact]
		public void Constructor_ArgumentIsNull_Throws() {
			// assert
			Assert.Throws<ArgumentNullException>(() => new LibraryController(null));
		}

		[Fact]
		public async Task GetBooksAsync_ServiceCalled() {
			// arrange
			List<MBook> books = new List<MBook>() {
				new MBook()
			};

			_repository.Query().ReturnsForAnyArgs(books.AsQueryable());

			// act
			var result = await _controller.GetBooksAsync();

			// assert
			await _manager.Received(1).GetBooksAsync(0, 50);
			result.Should().NotBeNull();
			result.Count().Should().Be(1);
		}
	}
}
