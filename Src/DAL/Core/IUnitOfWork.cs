﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Library.DAL.Core {
    /// <summary>
    /// Maintains a list of objects affected by a business transaction and coordinates the writing out of changes and the resolution of concurrency problems
    /// <see>https://martinfowler.com/eaaCatalog/unitOfWork.html</see>
    /// <see>https://stackoverflow.com/questions/74140462/implementing-repositories-with-ef-core-without-creating-multiples-methods</see>
    /// </summary>
    public interface IUnitOfWork : IDisposable { //IAsyncDisposable
		IRepositoryAsync<TEntity> RepoAsync<TEntity>() where TEntity : class, IEntity;
		Task<int> Commit();
		IMemoryCache Cache { get; }
	}

	public interface IUnitOfWork<TContext> : IUnitOfWork where TContext : DbContext {
		TContext Context { get; }
	}
}
