﻿using System.Threading;
using TechTalk.SpecFlow;

namespace Library.Automation.Test.Steps {
	public partial class SpecFlowSteps {
		[StepDefinition(@"#FILL: components")]
		public void ThenFillComponents(Table table) {
			foreach (var row in table.Rows) {
				var element = _engine.GetElementById(row[0]);
				element.SendKeys(row[1]);
			}
			Thread.Sleep(1 * 1000);
		}
	}
}