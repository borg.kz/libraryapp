﻿using FluentAssertions;
using Library.BLL.Managers;
using Library.DAL.Core;
using NSubstitute;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.DAL.Model;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace Library.Test.Managers {
	public class LibraryManagerTest {
		private readonly IRepositoryAsync<MBook> _repository;
		private readonly IUnitOfWork _uow;
		private readonly LibraryManager _manager;

        public LibraryManagerTest()
        {
            _repository = Substitute.For<IRepositoryAsync<MBook>>();
            _uow = Substitute.For<IUnitOfWork>();
            _uow.RepoAsync<MBook>().Returns(_repository);

            _manager = new LibraryManager(_uow);
        }

		[Fact]
		public void Constructor_ArgumentIsNull_Throws() {
			// assert
			Assert.Throws<ArgumentNullException>(() => new LibraryManager(null));
		}

        [Fact]
        public async Task GetBooksAsync_ValidData_OK()
        {
            // arrange
            List<MBook> books = new List<MBook>() { new MBook() };
            _repository.Query().ReturnsForAnyArgs(books.AsQueryable());

			// act
			var result = await _manager.GetBooksAsync(0, 50);

			// assert
			result.Should().NotBeNull();
			result.Count().Should().Be(1);
		}
	}
}
