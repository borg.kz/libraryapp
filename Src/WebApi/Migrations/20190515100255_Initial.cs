﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Library.WebApi.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    FullName = table.Column<string>(type: "nvarchar(150)", nullable: true),
                    LibraryUserId = table.Column<string>(type: "nvarchar(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Library_Author",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    Name = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_Author", x => x.Uid);
                });

            migrationBuilder.CreateTable(
                name: "Library_BookStore",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_BookStore", x => x.Uid);
                });

            migrationBuilder.CreateTable(
                name: "Library_RentRule",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FirstCost = table.Column<float>(nullable: false),
                    FirstCostPeriod = table.Column<int>(nullable: false),
                    SecondCost = table.Column<float>(nullable: false),
                    SecondCostPeriod = table.Column<int>(nullable: false),
                    ThirdCost = table.Column<float>(nullable: false),
                    ThirdCostPeriod = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_RentRule", x => x.Uid);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderKey = table.Column<string>(maxLength: 128, nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(maxLength: 128, nullable: false),
                    Name = table.Column<string>(maxLength: 128, nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Library_Book",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    Title = table.Column<string>(maxLength: 250, nullable: false),
                    ISBN = table.Column<string>(maxLength: 13, nullable: false),
                    Publisher = table.Column<string>(maxLength: 100, nullable: false),
                    PublishingDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(maxLength: 400, nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    RentRuleUid = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_Book", x => x.Uid);
                    table.ForeignKey(
                        name: "FK_Library_Book_Library_RentRule_RentRuleUid",
                        column: x => x.RentRuleUid,
                        principalTable: "Library_RentRule",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Library_BorrowingHistory",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    BookUid = table.Column<Guid>(nullable: false),
                    DateOfGetting = table.Column<DateTime>(nullable: false),
                    DateOfReturning = table.Column<DateTime>(nullable: true),
                    PresumeDateOfReturn = table.Column<DateTime>(nullable: false),
                    RentRuleUid = table.Column<Guid>(nullable: true),
                    ManagerId = table.Column<string>(nullable: true),
                    ClientId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_BorrowingHistory", x => x.Uid);
                    table.ForeignKey(
                        name: "FK_Library_BorrowingHistory_Library_Book_BookUid",
                        column: x => x.BookUid,
                        principalTable: "Library_Book",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Library_BorrowingHistory_AspNetUsers_ClientId",
                        column: x => x.ClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Library_BorrowingHistory_AspNetUsers_ManagerId",
                        column: x => x.ManagerId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Library_BorrowingHistory_Library_RentRule_RentRuleUid",
                        column: x => x.RentRuleUid,
                        principalTable: "Library_RentRule",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Library_StoreHistory",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Count = table.Column<int>(nullable: false),
                    BookUid = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_StoreHistory", x => x.Uid);
                    table.ForeignKey(
                        name: "FK_Library_StoreHistory_Library_Book_BookUid",
                        column: x => x.BookUid,
                        principalTable: "Library_Book",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Library_WaitingQeue",
                columns: table => new
                {
                    Uid = table.Column<Guid>(nullable: false),
                    Version = table.Column<byte[]>(type: "timestamp", rowVersion: true, nullable: false),
                    BookUid = table.Column<Guid>(nullable: false),
                    ClientId = table.Column<string>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Library_WaitingQeue", x => x.Uid);
                    table.ForeignKey(
                        name: "FK_Library_WaitingQeue_Library_Book_BookUid",
                        column: x => x.BookUid,
                        principalTable: "Library_Book",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Library_WaitingQeue_AspNetUsers_ClientId",
                        column: x => x.ClientId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Rel_Book_Author",
                columns: table => new
                {
                    BookId = table.Column<Guid>(nullable: false),
                    AuthorId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rel_Book_Author", x => new { x.BookId, x.AuthorId });
                    table.ForeignKey(
                        name: "FK_Rel_Book_Author_Library_Author_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Library_Author",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Rel_Book_Author_Library_Book_BookId",
                        column: x => x.BookId,
                        principalTable: "Library_Book",
                        principalColumn: "Uid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Library_Book_RentRuleUid",
                table: "Library_Book",
                column: "RentRuleUid");

            migrationBuilder.CreateIndex(
                name: "IX_Library_BorrowingHistory_BookUid",
                table: "Library_BorrowingHistory",
                column: "BookUid");

            migrationBuilder.CreateIndex(
                name: "IX_Library_BorrowingHistory_ClientId",
                table: "Library_BorrowingHistory",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Library_BorrowingHistory_ManagerId",
                table: "Library_BorrowingHistory",
                column: "ManagerId");

            migrationBuilder.CreateIndex(
                name: "IX_Library_BorrowingHistory_RentRuleUid",
                table: "Library_BorrowingHistory",
                column: "RentRuleUid");

            migrationBuilder.CreateIndex(
                name: "IX_Library_StoreHistory_BookUid",
                table: "Library_StoreHistory",
                column: "BookUid");

            migrationBuilder.CreateIndex(
                name: "IX_Library_WaitingQeue_BookUid",
                table: "Library_WaitingQeue",
                column: "BookUid");

            migrationBuilder.CreateIndex(
                name: "IX_Library_WaitingQeue_ClientId",
                table: "Library_WaitingQeue",
                column: "ClientId");

            migrationBuilder.CreateIndex(
                name: "IX_Rel_Book_Author_AuthorId",
                table: "Rel_Book_Author",
                column: "AuthorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Library_BookStore");

            migrationBuilder.DropTable(
                name: "Library_BorrowingHistory");

            migrationBuilder.DropTable(
                name: "Library_StoreHistory");

            migrationBuilder.DropTable(
                name: "Library_WaitingQeue");

            migrationBuilder.DropTable(
                name: "Rel_Book_Author");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Library_Author");

            migrationBuilder.DropTable(
                name: "Library_Book");

            migrationBuilder.DropTable(
                name: "Library_RentRule");
        }
    }
}
