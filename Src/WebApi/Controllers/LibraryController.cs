﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Library.BLL.DTO;
using Library.BLL.Managers;
using Library.DAL;
using Library.DAL.Auth;
using Library.WebApi.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Library.WebAPI.Controllers {
	[Route("[controller]")]
	[ApiController]
	public class LibraryController : ControllerBase {
		private readonly LibraryManager _manager;

		// TODO Arch#7: We can add add-hoc returning object { "Error": "", "Result: "" } and handle it on UI layer
		public LibraryController(LibraryManager manager) {
			_manager = manager ?? throw new ArgumentNullException(nameof(manager));
		}

		[HttpGet]
		public ActionResult<DateTime> HealthCheck() {
			return DateTime.Now;
		}

		[HttpGet("GetBooksAsync")]
		// [Authorize]
		public Task<IEnumerable<BookDto>> GetBooksAsync() {
			return _manager.GetBooksAsync(0, 50);
		}

		[HttpGet("GetBookInfo")]
		[Authorize]
		public Task<BookDto> GetBookInfoAsync(string uid) {
			return _manager.GetBookInfoAsync(uid);
		}

		[HttpPost("AddBook")]
		// [Authorize(Roles = Role.Manager)]
		public Task<bool> AddBookAsync(BookDto book) {
			if (!book.PublishDate.Contains('/'))
				throw new ArgumentException(nameof(book.PublishDate));

			book.PublishDate = book.PublishDate.Replace("/", "/01/");
			return _manager.AddBookAsync(book);
		}
		
		[Route("UpdateBook")]
		[HttpPut]
		[Authorize(Roles = Role.Manager)]
		public Task<bool> UpdateBookAsync(BookDto book) {
			return _manager.UpdateBookAsync(book);
		}

		[Route("RentBook")]
		[HttpPost]
		[Authorize(Roles = Role.Manager)]
		public Task<bool> RentBookAsync(RentBookDto rentBook) {
			var managerId = User.Claims.First(c => c.Type == "UserID").Value;
			return _manager.RentBookAsync(rentBook, managerId);
		}

		[Route("WrittingOffBook")]
		[HttpPost]
		[Authorize(Roles = Role.Manager)]
		public Task<bool> WrittingOffBookAsync(BookDto book) {
			return _manager.WrittingOffBookAsync(book);
		}

		[Route("GetRenredBooks")]
		[HttpGet]
		[Authorize(Roles = Role.Manager)]
		public Task<bool> GetRenredBooksAsync(int skip, int take) {
			return _manager.GetRenredBooksAsync(skip, take);
		}

		[Route("ReturnBook")]
		[HttpPut]
		[Authorize(Roles = Role.Manager)]
		public Task<bool> ReturnBookAsync(ReturnBookDto obj) {
			return _manager.ReturnBookAsync(obj);
		}

		[Route("GetCostOfRent")]
		[HttpPost]
		[Authorize(Roles = Role.Manager)]
		public Task<float> GetCostOfRentAsync(ReturnBookDto obj) {
			return _manager.CostOfRentAsync(obj);
		}

		[Route("BookSearch")]
		[HttpPut]
		[Authorize(Roles = Role.Manager)]
		public Task<IEnumerable<BookDto>> BookSearchAsync(BookSearchDto query) {
			return _manager.BookSearchAsync(query);
		}
	}
}