﻿Feature: LoginPage

@smoke
Scenario: LoginForm_check_inputs_sucsess
	Given ~NAVIGATE: to page 'LoginPage'
		And #FIND: components is existing
			| id         |
			| UserNameId |
			| PasswordId |
			| SubmitId   |
		And #FIND: count '2' of form 'formId' components
		And #CHECK: element 'SubmitId' is diabled 'true'
	When #FILL: components
		| id         | value  |
		| UserNameId | Admin  |
		And #CHECK: element 'SubmitId' is diabled 'true'
		And #FILL: components
		| id         | value  |
		| PasswordId | 12 |
		And #CHECK: element 'SubmitId' is diabled 'true'
		And #FILL: components
		| id         | value  |
		| PasswordId | 123456 |
		And #CHECK: element 'SubmitId' is diabled 'false'
	Then @ACTION: click 'SubmitId'

@smoke
Scenario: LoginForm_fill_inputs_sucsess 

Given #NAVIGATE: to URL 'http:\\localhost:4200'
	And #FILL: components
	| id         | value |
	| UserNameId | User1 |
	| PasswordId | 123456 |
	And #FIND: components is existing
	| id       |
	| SubmitId |
	And @ACTION: click 'SubmitId'

Scenario: LoginForm_log_in_sucsess

Given #NAVIGATE: to URL 'http:\\localhost:4200'
	And @ACTION: click 'SubmitId'

Scenario: LoginForm_log_in_fail

Given #NAVIGATE: to URL 'http:\\localhost:4200'
	And @ACTION: click 'SubmitId'