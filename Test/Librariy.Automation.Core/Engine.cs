﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Opera;
using OpenQA.Selenium.Remote;

namespace Librariy.Automation.Core {
	public enum BrowserType {
		Chrome = 1,
		Edge= 2,
		IE= 3,
		FF= 4,
		Opera= 5,
		Remote = 6
	}

	public sealed class Engine : IDisposable {
		public IWebDriver Browser { get; private set; }
		public BrowserType Type { get; }

		private bool _disposed = false;
		private Actions _actions;
		private IJavaScriptExecutor _jsExecutor;

		public Engine() {
			Type = BrowserType.Chrome;
			Start();
		}

		~Engine() {
			if (!_disposed)
				Dispose();
		}

		public void Dispose() {
			if (_disposed)
				return;

			Browser.Quit();
			Browser.Dispose();
			Browser = null;
			KillProcesses();
			_disposed = true;
			GC.SuppressFinalize(this);
		}

		public void Restart() {
			Dispose();
			Start();
		}

		private void Start() {
			switch (Type) {
				case BrowserType.Chrome: 
					Browser = InitializeChrome();
					break;
				case BrowserType.Edge:
					Browser = InitializeEdge();
					break;
				case BrowserType.IE:
					Browser = InitializeIE();
					break;
				case BrowserType.FF:
					Browser = new FirefoxDriver();
					break;
				case BrowserType.Opera:
					Browser = new OperaDriver();
					break;
				default:
					Browser = new RemoteWebDriver(new RemoteSessionSettings());
					break;
			}
			_actions = new Actions(Browser);
			_jsExecutor = Browser as IJavaScriptExecutor;
		}

		#region IWebElement events

		public void Click(IWebElement element, byte waitSecondsAfter = 5) {
			_actions.Click(element).Build().Perform();
			// Browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(waitSecondsAfter);
			Thread.Sleep(waitSecondsAfter * 1000);
		}

		public void DoubleClick(IWebElement element, byte waitSecondsAfter = 5) {
			_actions.DoubleClick(element).Build().Perform();
			// Browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(waitSecondsAfter);
			Thread.Sleep(waitSecondsAfter * 1000);
		}

		public void Blur(IWebElement element) {
			_jsExecutor.ExecuteScript("$(arguments[0]).blur()", element);
		}

		public void ScroleToElement(IWebElement element) {
			_actions.MoveToElement(element).Perform();
		}

		public IWebElement GetActiveWebElement() {
			return Browser.SwitchTo().ActiveElement();
		}

		#endregion

		#region IWebElement DOM requests

		public IWebElement GetParent(IWebElement element) {
			return element.FindElement(By.XPath(".."));
		}

		public IWebElement GetElementById(string elementId) {
			return Browser.FindElement(By.Id(elementId));
		}

		#endregion

		public void KillProcesses() {
			string name = string.Empty;
			switch (Type) {
				case BrowserType.Chrome: name = "Chrome";
					break;
				case BrowserType.IE: name = "IEDriverServer";
					break;
			}

			var processes = Process.GetProcessesByName(name);
			foreach (var process in processes) {
				process.Kill();
			}
		}

		private IWebDriver InitializeIE() {
			InternetExplorerOptions options = new InternetExplorerOptions() {
				EnsureCleanSession = true,
				PageLoadStrategy = PageLoadStrategy.Normal
			};

			return new InternetExplorerDriver(options);
		}

		private IWebDriver InitializeEdge() {
			return new EdgeDriver();
		}

		private IWebDriver InitializeChrome() {
			ChromeOptions options = new ChromeOptions();

			return new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
		}
	}
}