import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { RegistrationComponent } from './user/registration/registration.component';
import { UserService } from './shared/user.service';
import { LoginComponent } from './user/login/login.component';
import { HomeComponent } from './home/home.component';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';
import { StoreComponent } from './store/store.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { BookStoreComponent } from './book-store/book-store.component';
import { BookStoreAddComponent } from './book-store-add/book-store-add.component';
import { BookStoreRentComponent } from './book-store-rent/book-store-rent.component';
import { BookStoreWriteOffComponent } from './book-store-write-off/book-store-write-off.component';
import { ClientBookSearchComponent } from './client-book-search/client-book-search.component';
import { BookStoreReturnComponent } from './book-store-return/book-store-return.component';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    RegistrationComponent,
    LoginComponent,
    HomeComponent,
    AdminPanelComponent,
    ForbiddenComponent,
    StoreComponent,
    NavMenuComponent,
    BookStoreComponent,
    BookStoreAddComponent,
    BookStoreRentComponent,
    BookStoreWriteOffComponent,
    ClientBookSearchComponent,
    BookStoreReturnComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      progressBar: true
    }),
    FormsModule
  ],
  providers: [UserService, {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
