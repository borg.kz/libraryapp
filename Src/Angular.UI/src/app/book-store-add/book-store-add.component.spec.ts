import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookStoreAddComponent } from './book-store-add.component';

describe('BookStoreAddComponent', () => {
  let component: BookStoreAddComponent;
  let fixture: ComponentFixture<BookStoreAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookStoreAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
