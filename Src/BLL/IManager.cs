﻿using Library.DAL.Core;

namespace Library.BLL {
	public interface IManager {
		IUnitOfWork Uow { get; }
	}
}