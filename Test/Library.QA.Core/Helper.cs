﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Epic.SCF.QA.Core
{
    public static class Helper
    {
        public static void Debug(string message)
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now.ToString() + $"# {message}");
        }
    }
}
