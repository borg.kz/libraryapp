﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	[Table("Library_Book")]
	public class MBook : EntityBase {
		[Required, MaxLength(250)]
		public string Title{ get; set; }
		
		[Required, MaxLength(13)]
		public string ISBN{ get; set; }
		
		[Required, MaxLength(100)]
		public string Publisher{ get; set; }
		
		[Required]
		public DateTime PublishingDate{ get; set; }
		
		[MaxLength(400)]
		public string Description{ get; set; }

		[Required]
		public int Quantity{ get; set; }

		public MRentRule RentRule { get; set; }

		public ICollection<MRelBookAuthor> BookAutor { get; set; }

		public ICollection<MBorrowingHistory> BorrowingHistory { get; set; }
	}
}