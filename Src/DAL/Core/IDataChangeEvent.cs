﻿using System;

namespace Library.DAL.Core {
	public interface IDataChangeEvent {
		Type Type { get; }
		Action<Guid, IEntity> Action { get; }
	}
}