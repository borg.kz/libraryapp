﻿using Library.DAL.Auth;
using Library.DAL.Model;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Library.DAL.Context {
	public class LibraryContext : IdentityDbContext {
		public LibraryContext(DbContextOptions options) : base(options) {}

		public DbSet<MUser> AppUsers { get; set; }
		public DbSet<MAuthor> Autors { get; set; }
		public DbSet<MBook> Books { get; set; }
		public DbSet<MBookStore> BooksStore { get; set; }
		public DbSet<MBorrowingHistory> BorrowingHistoriy { get; set; }
		public DbSet<MRentRule> Rules { get; set; }
		public DbSet<MStoreHistory> StoreHistoriy { get; set; }
		public DbSet<MWaitingQeue> WaitingQeue { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			base.OnModelCreating(modelBuilder);

			// modelBuilder.Entity<YourEntity>().Property(x => x.Id).HasDefaultValueSql("NEWID()");

			modelBuilder.Entity<MBook>() 
				.HasOne(pt => pt.RentRule) 
				.WithMany(t => t.Books).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<MBorrowingHistory>()
				.HasOne(p => p.Manager)
				.WithMany(m => m.LendingHistory).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<MBorrowingHistory>()
				.HasOne(p => p.Client)
				.WithMany(m => m.BorrowingHistory).OnDelete(DeleteBehavior.Restrict);

			modelBuilder.Entity<MBorrowingHistory>()
				.HasOne(p => p.Book)
				.WithMany(b => b.BorrowingHistory).OnDelete(DeleteBehavior.Cascade);

			// Add many-to-many relation Boos->Authors
			modelBuilder.Entity<MRelBookAuthor>()
				.HasKey(x => new {x.BookId, x.AuthorId});

			modelBuilder.Entity<MRelBookAuthor>() 
				.HasOne(pt => pt.Book)
				.WithMany(p => p.BookAutor)
				.HasForeignKey(pt => pt.BookId);
 
			modelBuilder.Entity<MRelBookAuthor>() 
				.HasOne(pt => pt.Author) 
				.WithMany(t => t.BookAutor)
				.HasForeignKey(pt => pt.AuthorId);
		}
	}
}