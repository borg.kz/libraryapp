﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query;

namespace Library.DAL.Core {
	public interface IRepositoryAsync<T> where T : class, IEntity {
		List<Action<Guid, IEntity>> DataChangeActions { get; set; }

		Task<T> FirstOrDefault(
			Expression<Func<T, bool>> predicate = null,
			Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null);

		// TODO Arch#4: Add reloader method for getting All entities with skip=null, take=null, orderBy=null
		Task<IQueryable<T>> Query(
			Expression<Func<T, bool>> predicate = null,
			Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null,
			Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
			int? skip = null,
			int? take = null);

		Task Create(T entity, CancellationToken cancellationToken = default(CancellationToken));

		Task Update(T entity);

		Task Delete(T entity);

		Task<int> Commit();

		void MarkUnchanged(IEntity entity);
	}
}