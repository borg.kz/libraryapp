using System;
using System.Text;
using Library.BLL.Managers;
using Library.DAL.Auth;
using Library.DAL.Context;
using Library.DAL.Core;
using Library.WebApi;
using Library.WebApi.ActionFilters;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Serialization;

namespace Library.WebAPI {
	public class Startup {
		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services) {
			//Inject AppSettings
			services.Configure<ApplicationSettings>(Configuration.GetSection("ApplicationSettings"));

			services.AddMvc(options => {
                    options.Filters.Add(new OnUnhandledExeptionHandler(typeof(ArgumentNullException), 400));
                    options.Filters.Add(new OnUnhandledExeptionHandler(typeof(ArgumentException), 400));
                    options.Filters.Add(new OnUnhandledExeptionHandler(typeof(NullReferenceException), 400));
                })
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
				.AddJsonOptions(options => {
					var resolver = options.SerializerSettings.ContractResolver;
					if (resolver != null && resolver is DefaultContractResolver r)
						r.NamingStrategy = null;
				});

			services.AddDbContext<LibraryContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("Default"),  b => b.MigrationsAssembly("Library.WebApi")));

			services.AddDefaultIdentity<MUser>()
				.AddRoles<IdentityRole>()
				.AddEntityFrameworkStores<LibraryContext>();

			services.AddScoped<IUnitOfWork, UnitOfWork<LibraryContext>>();

			services.Configure<IdentityOptions>(
				options => {
					options.Password.RequireDigit = false;
					options.Password.RequireNonAlphanumeric = false;
					options.Password.RequireLowercase = false;
					options.Password.RequireUppercase = false;
					options.Password.RequiredLength = 4;
				}
			);

			services.AddCors();
			services.AddDistributedMemoryCache();

			#region Meneagers

			services.AddTransient<LibraryManager>();
			
			#endregion Meneagers

			//Jwt Authentication
			var key = Encoding.UTF8.GetBytes(Configuration["ApplicationSettings:JWT_Secret"]);

			services.AddAuthentication(
				x => {
					x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
					x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
					x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
				}).AddJwtBearer(
				x => {
					x.RequireHttpsMetadata = false;
					x.SaveToken = false;
					x.TokenValidationParameters = new TokenValidationParameters {
						ValidateIssuerSigningKey = true,
						IssuerSigningKey = new SymmetricSecurityKey(key),
						ValidateIssuer = false,
						ValidateAudience = false,
						ClockSkew = TimeSpan.Zero
					};
				});
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
			app.Use(
				async (ctx, next) => {
					await next();
					if (ctx.Response.StatusCode == 204) {
						ctx.Response.ContentLength = 0;
					}
				});

			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}

			app.UseCors(
				builder =>
					builder.WithOrigins(Configuration["ApplicationSettings:Client_URL"].ToString())
						.AllowAnyHeader()
						.AllowAnyMethod()
			);

			app.UseAuthentication();
			app.UseMvc();
		}
	}
}