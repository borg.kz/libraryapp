﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace Library.BLL.DTO {
	public class BookSearchDto {
		public string Isbn { get; set; }
		public string Author { get; set; }
		public string Publisher { get; set; }
		public string Description { get; set; }
	}
}
