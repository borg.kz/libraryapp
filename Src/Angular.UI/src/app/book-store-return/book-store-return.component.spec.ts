import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookStoreReturnComponent } from './book-store-return.component';

describe('BookStoreReturnComponent', () => {
  let component: BookStoreReturnComponent;
  let fixture: ComponentFixture<BookStoreReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookStoreReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookStoreReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
