import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { BookStoreService } from '../book-store.service';
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-book-store-return',
  templateUrl: './book-store-return.component.html'
})
export class BookStoreReturnComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private pbookStoreService: BookStoreService, private toastr: ToastrService) { }
  
  addForm: FormGroup;
  submitted = false;
  
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      Uid: [],
      Isbn: ['', Validators.required],
      Lui: ['', Validators.required],
      CostOfRent: ['']
    });
  }

  onCheckCost() {
    if(this.addForm.valid){
      this.pbookStoreService.checkCost(this.addForm.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.info('Cost of rent', data + ' Euros');
        return;
      });
    }
  }

  onSubmit() {
    this.submitted = true;
    
    if(this.addForm.valid){
      this.pbookStoreService.returnBook(this.addForm.value)
      .subscribe( data => {
        console.log(data);
        this.toastr.info('Info', 'Book was returned');
        // We can check answer here, response have to have 200 status code, 
        // and we can show Error message from server, if not 200 code is happened 
        // gloal function CheckStatus(data)
      });
    }
  }

  // get the form short name to access the form fields
  get f() { return this.addForm.controls; }
}
