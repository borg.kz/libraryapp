﻿using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Xunit;

namespace Library.Automation.Test.Steps {
	public partial class SpecFlowSteps {
		/// <summary>
		/// Find all components by Id and check it on existing
		/// </summary>
		/// <param name="table">Ids of components</param>
		[StepDefinition("#FIND: components is existing")]
		public void GivenFindComponents(Table table) {
			foreach (var row in table.Rows)
				Assert.True(_engine.GetElementById(row["id"]) != null, $"Element {row["id"]} not exist. ({_pageUrl})");
		}

		[StepDefinition(@"#FIND: count '(\d*)' of form '(.*)' components")]
		public void GivenFindFormComponentsCount(int conponentsCount, string formId) {
			
		}
	}
}
