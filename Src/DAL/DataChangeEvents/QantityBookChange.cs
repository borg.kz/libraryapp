﻿using System;
using Library.DAL.Core;
using Library.DAL.Model;

namespace Library.DAL.DataChangeEvents {
	// TODO Arch#2: Generic approach can be added to IDataChangeEvent
	// TODO Arch#5: We can add specific type of data changes for event: create, update, delete
	public class QantityBookChange : IDataChangeEvent {
		private readonly IUnitOfWork _uow;

		// TODO Arch#1: We can change Type on List<Type> and then we could use 1 IDataChangeEvent for multiply Entities
		public Type Type { get; } = typeof(MBook);
		public Action<Guid, IEntity> Action => ChangeData;

		public QantityBookChange(IUnitOfWork uow) {
			_uow = uow;
		}

		public async void ChangeData(Guid guid, IEntity entity) {
			return;
			// TODO: we can use any additional actions(source code) here 
			// If you need the changing object unbox entity to neaded type example (MStoreHistory)entity
		}
	}
}