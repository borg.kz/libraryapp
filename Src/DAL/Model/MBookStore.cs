﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	// TODO: Actually we can remove it and move fields to MBook entity or move MBorrowingHistory from MBook to this class
	[Table("Library_BookStore")]
	[Obsolete("Should be deleted")]
	public class MBookStore : EntityBase {

	}
}