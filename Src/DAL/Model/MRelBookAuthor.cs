﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	[Table("Rel_Book_Author")]
	public class MRelBookAuthor : IEntity {
		public Guid BookId { get; set; }
		public Guid AuthorId { get; set; }

		public MBook Book { get; set; }
		public MAuthor Author { get; set; }

		[NotMapped]
		public Guid Uid { get; set; }
		[NotMapped]
		public byte[] Version { get; set; }
	}
}