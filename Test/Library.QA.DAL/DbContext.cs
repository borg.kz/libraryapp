﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Epic.SCF.QA.DAL
{
    // TODO: https://www.entityframeworktutorial.net/
    public partial class ScfDbContext : DbContext
    {
        public ScfDbContext()
        {
        }

        public virtual DbSet<CodeRow> CodeRow { get; set; }
        public virtual DbSet<Invoice> Invoice { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(@"Server=LVAGPLTP2990\SQLEXPRESS;Database=SCFDB;Trusted_Connection=True;");
            }
        }
    }
}
