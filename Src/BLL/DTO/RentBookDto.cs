﻿namespace Library.BLL.DTO {
	public class RentBookDto {
		/// <summary> Book Isbn </summary>
		public string Isbn { get; set; }
		/// <summary> Library user identificier</summary>
		public string Lui { get; set; }
	}
}