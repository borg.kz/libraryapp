﻿using Library.DAL.Core;

namespace Library.BLL {
	public abstract class ManagerBase : IManager {
		public IUnitOfWork Uow { get; }

		protected ManagerBase(IUnitOfWork uow) {
            Uow = uow ?? throw new System.ArgumentNullException(nameof(uow));
        }
	}
}