import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from "@angular/forms";
import { BookStoreService } from '../book-store.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-book-store-add',
  templateUrl: './book-store-add.component.html'
})

export class BookStoreAddComponent implements OnInit {
  constructor(private formBuilder: FormBuilder, private router: Router, private pbookStoreService: BookStoreService) { }
  
  addForm: FormGroup;
  submitted = false;
  
  ngOnInit() {
    this.addForm = this.formBuilder.group({
      Uid: [],
      Title: ['', Validators.required],
      Description: ['', Validators.required],
      RentRule: ['', Validators.required],
      Publisher: ['', Validators.required],
      PublishDate: ['', Validators.required],
      Authors: ['', Validators.required],    
      Isbn: ['', Validators.required]});
  }
  onSubmit() {
    this.submitted = true;
    
    if (this.addForm.valid){
      this.pbookStoreService.addBook(this.addForm.value)
      .subscribe( data => {
        console.log(data);
        // TODO Arch#2: We can check answer here, response have to have 200 status code, 
        // and we can show Error message from server, if not 200 code is happened 
        // gloal function CheckStatus(data)
        this.router.navigate(['bookstore']);
      });
    }
  }

  // get the form short name to access the form fields
  get f() { return this.addForm.controls; }
}
