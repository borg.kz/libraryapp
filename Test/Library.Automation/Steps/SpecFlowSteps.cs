﻿using System;
using System.Threading;
using Librariy.Automation.Core;
using OpenQA.Selenium;
using TechTalk.SpecFlow;
using Xunit;

namespace Library.Automation.Test.Steps {
	// TODO: Probably the best solution will move all steps to the Core project. !!!!
	[Binding]
	public sealed partial class SpecFlowSteps {
		private static Engine _engine;
		private readonly ScenarioContext _context;
		private string _pageUrl = string.Empty;

		public SpecFlowSteps(ScenarioContext injectedContext) {
			_context = injectedContext;
		}

		// TODO: We can create Before/After Feature, Before/After Scenario, Before/After step action here
		[BeforeFeature()]
		public static void BeforeFeature() {
			_engine = new Engine();
		}
		
		[AfterFeature()]
		public static void AfterFeature() {
			_engine.Dispose();
			_engine = null;
		}

		[Given(@"#NAVIGATE: to URL '(http:\\.*)'")]
		public void NavigateToUrl(string url) {
			_pageUrl = url;
			_engine.Browser.Navigate().GoToUrl(url);
		}

		[Given(@"~NAVIGATE: to page '(\S*Page)'")]
		public void NavigateToPage(string page) {
			if (!Enum.TryParse(page, out EMapOfPages pageToNavigate))
				throw new ArgumentException($"Page {page} is not found!");

			string url = GetPageUrl(pageToNavigate);
			_pageUrl = url;
			_engine.Browser.Navigate().GoToUrl(url);
			Thread.Sleep(1 * 1000);
		}

		private string GetPageUrl(EMapOfPages pageToNavigate) {
			switch (pageToNavigate) {
				case EMapOfPages.LoginPage: return "http:\\localhost:4200";
				default:
					throw new ArgumentOutOfRangeException(nameof(pageToNavigate), pageToNavigate, null);
			}
		}
	}
}