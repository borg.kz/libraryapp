﻿using Epic.SCF.QA.Core.Engine;
using TechTalk.SpecFlow;

namespace Epic.SCF.QA.Infrastructur.Steps.Storage
{
    /// <summary>
    /// Navigation step
    /// </summary>
    [Binding]
    public class NavigateStep
    {
        private WebDriver _engine;

        public NavigateStep(WebDriver engine)
        {
            _engine = engine;
        }

        [Given(@"NAVIGATE: Enter to Google")]
        public void EnterToApplication()
        {
            _engine.Browser.Navigate().GoToUrl("Scf host name");
        }

        [Given(@"NAVIGATE: to URL '(http:\\.*)'")]
        public void NavigateToUrl(string url)
        {
            _engine.Browser.Navigate().GoToUrl(url);
        }

        [Given(@"NAVIGATE: TestPage")]
        public void TestPage()
        {
            _engine.Browser.Navigate().GoToUrl("google.com");
        }
    }
}
