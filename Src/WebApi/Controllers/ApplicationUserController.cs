﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Library.DAL.Auth;
using Library.WebApi;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Library.WebAPI.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class ApplicationUserController : ControllerBase {
		private UserManager<MUser> _userManager;
		private SignInManager<MUser> _singInManager;
		private readonly ApplicationSettings _appSettings;

		public ApplicationUserController(UserManager<MUser> userManager, SignInManager<MUser> signInManager, IOptions<ApplicationSettings> appSettings) {
			_userManager = userManager;
			_singInManager = signInManager;
			_appSettings = appSettings.Value;
		}

		[HttpGet]
		public int HealthCheck() {
			return Environment.TickCount;
		}

		[HttpPost]
		[Route("Register")]
		public async Task<object> PostApplicationUser(ApplicationUserModel model) {
			model.Role = "Manager";
			var applicationUser = new MUser() {
				UserName = model.UserName,
				Email = model.Email,
				FullName = model.FullName,
				LibraryUserId = $"U{new Random(Environment.TickCount).Next(10000, 99999)}"
			};

			var result = await _userManager.CreateAsync(applicationUser, model.Password);
			await _userManager.AddToRoleAsync(applicationUser, model.Role);
			return Ok(result);
		}

		[HttpPost]
		[Route("Login")]
		public async Task<IActionResult> Login(LoginModel model) {
			var user = await _userManager.FindByNameAsync(model.UserName);
			if (user != null && await _userManager.CheckPasswordAsync(user, model.Password)) {
				//Get role assigned to the user
				var role = await _userManager.GetRolesAsync(user);
				IdentityOptions _options = new IdentityOptions();

				var tokenDescriptor = new SecurityTokenDescriptor {
					Subject = new ClaimsIdentity(
						new Claim[] {
							new Claim("UserID", user.Id),
							new Claim(_options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
						}),
					Expires = DateTime.UtcNow.AddDays(1),
					SigningCredentials = new SigningCredentials(
						new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSettings.JWT_Secret)),
						SecurityAlgorithms.HmacSha256Signature)
				};
				var tokenHandler = new JwtSecurityTokenHandler();
				var securityToken = tokenHandler.CreateToken(tokenDescriptor);
				var token = tokenHandler.WriteToken(securityToken);

				return Ok(new { token });
			} 


			return BadRequest(new { message = "Username or password is incorrect." });
		}
	}
}