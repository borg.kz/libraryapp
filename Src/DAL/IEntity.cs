﻿using System;

namespace Library.DAL {
	public interface IEntity {
		// I need move it to EntityBase, add EntityVersion
		Guid Uid { get; set; }

		byte[] Version { get; set; }
	}
}