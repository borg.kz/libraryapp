﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Library.DAL.Auth;
using Library.DAL.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Library.WebAPI.Controllers {
	[Route("api/[controller]")]
	[ApiController]
	public class UserProfileController : ControllerBase {
		private UserManager<MUser> _userManager;
		private readonly IUnitOfWork _uow;
		private readonly IMemoryCache _memoryCache;

		public UserProfileController(UserManager<MUser> userManager, IUnitOfWork uow, IMemoryCache memoryCache) {
			_userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
			_uow = uow ?? throw new ArgumentNullException(nameof(userManager));
			_memoryCache = memoryCache ?? throw new ArgumentNullException(nameof(userManager));
		}

		[HttpGet]
		[Authorize]
		//GET : /api/UserProfile
		public async Task<Object> GetUserProfile() {
			string userId = User.Claims.First(c => c.Type == "UserID").Value;
			
			// MemoryCache using for store data for Home page
			_memoryCache.TryGetValue(userId, out object cachObject);

			if (cachObject != null)
				return cachObject;

			var random = new Random(Environment.TickCount);
			var user = await _userManager.FindByIdAsync(userId);
			var cache = new {
				user.FullName,
				user.Email,
				user.UserName,
				// In the real application those values will be got thru UOW
				BooksInstore = random.Next(10, 10000),
				Clients = random.Next(1, 20),
				BorrowebBooks = random.Next(1, 50),
				Debt = (random.Next(200, 1500)/13).ToString("C")
			};

			_memoryCache.Set(userId, cache, new MemoryCacheEntryOptions { SlidingExpiration = TimeSpan.FromSeconds(120) });

			return cache;
		}
	}
}