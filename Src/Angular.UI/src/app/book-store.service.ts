import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BookModel } from './Models/BookModel';

@Injectable({
  providedIn: 'root'
})

export class BookStoreService {

  constructor(private http: HttpClient) { }

  baseurl: string = "https://localhost:44386/Library/";

  getBooks() {
    return this.http.get<BookModel[]>(this.baseurl + 'GetBooksAsync');
  }

  getBookInfo(uid: string) {
    return this.http.get<BookModel>(this.baseurl + 'GetBookInfo' + '/' + uid);
  }

  addBook(book: BookModel) {
    return this.http.post(this.baseurl + 'AddBook', book);
  }

  rentBook(book: BookModel) {
    return this.http.post(this.baseurl + 'RentBook', book);
  }

  returnBook(book: BookModel) {
    return this.http.put(this.baseurl + 'ReturnBook', book);
  }

  checkCost(book: BookModel) {
    return this.http.post(this.baseurl + 'GetCostOfRent', book);
  }

  writingOffBook(book: BookModel) {
    return this.http.post(this.baseurl + 'WrittingOffBook', book);
  }
}
