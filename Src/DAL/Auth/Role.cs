﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Library.DAL.Auth {
	public static class Role {
		public const string Manager = "Manager";
		public const string Client = "Client";
		public const string Admin = "Admin";
	}
}
