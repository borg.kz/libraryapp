﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Library.DAL.Auth;

namespace Library.DAL.Model {
	[Table("Library_BorrowingHistory")]
	public class MBorrowingHistory  : EntityBase  {
		[Required]
		public MBook Book { get; set; }

		[Required]
		public DateTime DateOfGetting { get; set; }
		public DateTime? DateOfReturning { get; set; }
		public DateTime PresumeDateOfReturn { get; set; }
		public MRentRule RentRule { get; set; }

		/// <summary> Person who lend a book </summary>
 		[InverseProperty("LendingHistory")]
		public MUser Manager { get; set; }

		/// <summary> Person who borrow a book  </summary>
		[InverseProperty("BorrowingHistory")]
		public MUser Client { get; set; }
	}
}