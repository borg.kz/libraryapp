﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.DAL.Model {
	[Table("Library_StoreHistory")]
	public class MStoreHistory : EntityBase {
		[Required]
		public ETransactionType TransactionType { get; set; }

		[Required]
		public DateTime Date { get; set; }

		[Required]
		public int Count { get; set; }

		[Required]
		public MBook Book { get; set; }
	}
}