﻿using System;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Threading.Tasks;

namespace Library.WebApi.ActionFilters
{
    public class OnUnhandledExeptionHandler : Microsoft.AspNetCore.Mvc.Filters.ExceptionFilterAttribute
    {
        private readonly Type expectedExceptionType;

        private readonly int httpStatusCode = 400;

        public OnUnhandledExeptionHandler(Type expectedExceptionType, int httpStatusCode)
        {
            this.expectedExceptionType = expectedExceptionType ?? throw new ArgumentNullException(nameof(expectedExceptionType));
            this.httpStatusCode = httpStatusCode;
        }

        public override void OnException(ExceptionContext context)
        {
            if (expectedExceptionType.IsInstanceOfType(context.Exception))
            {
                var responseStatusCode = (int)httpStatusCode;
                var response = new
                {
                    Code = responseStatusCode,
                    Name = httpStatusCode,
                    Message = context.Exception.Message
                };

                context.ExceptionHandled = true;
                context.HttpContext.Response.StatusCode = responseStatusCode;
                context.Result = new Microsoft.AspNetCore.Mvc.ObjectResult(response);
            }
        }
    }
}
