﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Library.DAL.Core {
	public class RepositoryAsync<T> : IRepositoryAsync<T> where T : class, IEntity {
		protected readonly DbContext _dbContext;
		protected readonly DbSet<T> _dbSet;
		public event Action ChangeData;
		public List<Action<Guid, IEntity>> DataChangeActions { get; set; } = new List<Action<Guid, IEntity>>();

		public RepositoryAsync(DbContext dbContext) {
			_dbContext = dbContext;
			_dbSet = _dbContext.Set<T>();
		}

		public virtual Task<T> FirstOrDefault(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null) {
			IQueryable<T> query = _dbSet.AsNoTracking();
			if (predicate != null) query = query.Where(predicate);
			if (include != null) query = include(query);
			return orderBy != null ? orderBy(query).FirstOrDefaultAsync() : query?.FirstOrDefaultAsync();
		}

		// TODO Arch#8: We need to add "Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null" for loading nested objects
		// TODO Arch#8: Wthout it I can't load and map object with nesten object easily and proper!
		// TODO Arch#8: After testing we can remove the TODO
		public virtual Task<IQueryable<T>> Query(Expression<Func<T, bool>> predicate = null, Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, int?skip = null, int? take = null) {
			IQueryable<T> query = _dbSet.AsNoTracking();
			if (predicate != null) query = query.Where(predicate);
			if (include != null) query = include(query);
			var result  = orderBy == null ? query : orderBy(query).AsQueryable();

			if (!skip.HasValue)
				skip = 0;

			return Task.FromResult(take.HasValue ? result.Skip(skip.Value).Take(take.Value) : query);
		}

		public virtual async Task Create(T entity, CancellationToken cancellationToken = default(CancellationToken)) {
			await _dbSet.AddAsync(entity, cancellationToken);
			await _dbContext.SaveChangesAsync(cancellationToken);
			OnChange();
			ApplyDataChangeActions(entity);
			if (_dbContext.ChangeTracker.HasChanges())
				await _dbContext.SaveChangesAsync(cancellationToken);
		}

		public virtual async Task Delete(T entity) {
			CheckOptimisticBlocking(entity);
			_dbSet.Remove(entity);
			await _dbContext.SaveChangesAsync();
			OnChange();
			ApplyDataChangeActions(entity);
			if (_dbContext.ChangeTracker.HasChanges())
				await _dbContext.SaveChangesAsync();
		}

		public virtual async Task Update(T entity) {
			CheckOptimisticBlocking(entity);
			_dbSet.Update(entity);
			await _dbContext.SaveChangesAsync();
			OnChange();
			ApplyDataChangeActions(entity);
			if (_dbContext.ChangeTracker.HasChanges())
				await _dbContext.SaveChangesAsync();
		}

		public virtual Task<int> Commit() {
			return _dbContext.SaveChangesAsync();
		}

		public void MarkUnchanged(IEntity entity) {
			_dbContext.Entry(entity).State = EntityState.Unchanged;
		}

		protected virtual void OnChange() {
			ChangeData?.Invoke();
		}

		protected virtual void CheckOptimisticBlocking(T entity) {
			IQueryable<T> query = _dbSet.AsNoTracking();
            // EF core 5/6
            // var sql = query.ToQueryString();

            var version = query.Where(e => e.Uid == entity.Uid).Select(
				s => new {
					s.Version
				}).FirstOrDefault();

			for (int i = 0; i < entity.Version.Length; i++) {
				if (entity.Version[i] != version?.Version[i])
					throw new DBConcurrencyException();
			}
		}

		protected virtual void ApplyDataChangeActions(IEntity entity) {
			foreach (var action in DataChangeActions) {
				action?.Invoke(entity.Uid, entity);
			}
		}
	}
}